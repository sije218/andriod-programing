package com.example.movieapp10.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp10.R
import com.example.movieapp10.fragment.movieGetter.TrendingRepository
import com.example.movieapp10.fragment.movieGetter.trendingAdapter


/**
 * A simple [Fragment] subclass.
 * Use the [TrendingFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TrendingFragment : Fragment() {
    lateinit var trenRecyc: RecyclerView
    lateinit var adapter: trendingAdapter
    private lateinit var trendingView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = trendingAdapter(listOf())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        trendingView = inflater.inflate(R.layout.fragment_trending, container,false)
        trenRecyc = trendingView.findViewById(R.id.TrenMovieRecyc)
        trenRecyc.setHasFixedSize(true)
        trenRecyc.layoutManager = GridLayoutManager(parentFragment?.context,1)
        trenRecyc.adapter = adapter
        return trendingView

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        TrendingRepository.getTrendingMovies(
                onSuccess = ::onTrenMoviesFetched,
                onError = ::onError
        )
    }
    private fun onTrenMoviesFetched(trendingMovies: List<com.example.movieapp10.fragment.movieGetter.Result>) {
        adapter.notifyDataSetChanged()
        adapter.updateTrending(trendingMovies)
        Log.d("RanMovie", "Movies: $trendingMovies")
    }
    private fun onError(){
        Toast.makeText(requireContext(), getString(R.string.FejlHentningAfFilm), Toast.LENGTH_SHORT).show()

    }
}