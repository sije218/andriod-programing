package com.example.movieapp10.fragment.movieGetter

import com.example.movieapp10.API
import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.net.URI
import kotlin.random.Random


////////////// Start - Random film, henter fra TMDb under populære film
data class Movie1(
        @SerializedName("id") val id: Long,
        @SerializedName("title") val title: String,
        @SerializedName("overview") val overview: String,
        @SerializedName("poster_path") val posterPath: String,
        @SerializedName("release_date") val releaseDate: String,
        @SerializedName("popularity") val popularity: String
)
data class GetMoviesResponse(
        @SerializedName("page") val page: Int,
        @SerializedName("results") val movies: List<Movie1>,
        @SerializedName("total_pages") val pages: Int
)

interface Api {

    @GET("movie/popular")
    fun getRanMovies(
            @Query("api_key") key: String = API,
            @Query("page") page: Int
    ): Call<GetMoviesResponse>

}

////////////// SLUT - Random film

////////////// Start - Trending film, henter film fra TMDb under trending, movies, day
data class Trending(
        val results: List<Result>
)
data class Result(
        val id: Int,
        val poster_path: String,
        val title: String,
        val overview: String
)

interface Trendingapi{
    @GET("trending/movie/day")
    fun getTrending(
            @Query("api_key") key: String = "8403d1ad0229a3c462fd27b3346b1e83",
            @Query("media_type") mediaType: String = "movie",
            @Query("time_window") timeWindow: String = "day"
    ): Call<Trending>
}
////////////// SLUT - Trending film


object TrendingRepository{
    private val api: Trendingapi
    init { val retrofit = Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        api = retrofit.create(Trendingapi::class.java)
    }
    fun getTrendingMovies(onSuccess: (List<Result>) -> Unit, onError: () -> Unit){
        api.getTrending().enqueue(object : Callback<Trending> {
            override fun onResponse(call: Call<Trending>, response: Response<Trending>) {
                if (response.isSuccessful) {
                    val responseBody = response.body()
                    if (responseBody != null){
                        onSuccess.invoke(responseBody.results)
                    }else{
                        onError.invoke()
                    }
                } else {
                    onError.invoke()
                }
            }
            override fun onFailure(call: Call<Trending>, t: Throwable) {
                onError.invoke()
            }
        })
    }
}

object MoviesRepository { //Content provider -  downloading film posters and titles.
    private val api: Api

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        api = retrofit.create(Api::class.java)
    }
    fun getRanMovies(page: Int = 2, onSuccess: (List<Movie1>) -> Unit, onError: () -> Unit) {
        api.getRanMovies(page = Random.nextInt(1,100))
                .enqueue(object : Callback<GetMoviesResponse> {
                    override fun onResponse(call: Call<GetMoviesResponse>, response: Response<GetMoviesResponse>) {
                        if (response.isSuccessful) {
                            val responseBody = response.body()
                            if (responseBody != null) {
                                onSuccess.invoke(responseBody.movies)
                            } else {
                                onError.invoke()
                            }
                        } else {
                            onError.invoke()
                        }
                    }
                    override fun onFailure(call: Call<GetMoviesResponse>, t: Throwable) {
                        onError.invoke()
                    }
                })
    }
}

