package com.example.movieapp10.fragment.movieGetter


import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieapp10.MovieInfo
import com.example.movieapp10.R


class MovieAdapter1(private var movies: List<Movie1>) : RecyclerView.Adapter<MovieAdapter1.MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_movie, parent, false)

        return MovieViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie1 = movies[position]
        holder.bind(movies[position])
        holder.itemView.setOnClickListener{ v ->
            val infoIntent = Intent(v.context, MovieInfo::class.java)
            infoIntent.putExtra("title", movie1.title)
            infoIntent.putExtra("overview", movie1.overview)
            infoIntent.putExtra("posterpath", movie1.posterPath)
            infoIntent.putExtra("releasedate", movie1.releaseDate)
            infoIntent.putExtra("popularity", movie1.popularity)
            v.context.startActivity(infoIntent)

        }
    }

    fun updateMovies(movies: List<Movie1>) {
        this.movies = movies
        notifyDataSetChanged()
    }

    inner class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val poster: ImageView = itemView.findViewById(R.id.item_movie_poster)
        private val title: TextView = itemView.findViewById(R.id.MovieName)
        fun bind(movie: Movie1) {
            Glide.with(itemView)
                    .load("https://image.tmdb.org/t/p/w185${movie.posterPath}")
                    .centerCrop()
                    .into(poster)
            title.text = movie.title
            Log.i("movieBinder", "$movie")
        }

    }
}
class trendingAdapter(var trendingMovies: List<Result>): RecyclerView.Adapter<trendingMovieViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): trendingMovieViewHolder {
        val trendingView = LayoutInflater.from(parent.context).inflate(R.layout.item_trending, parent, false)
        return trendingMovieViewHolder(trendingView)
    }

    override fun getItemCount(): Int {
        return trendingMovies.size
    }

    override fun onBindViewHolder(holder: trendingMovieViewHolder, position: Int) {
        return holder.bind(trendingMovies[position])
    }

    fun updateTrending(movies: List<Result>) {
        this.trendingMovies = movies
        notifyDataSetChanged()
    }

}

class trendingMovieViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    private val poster: ImageView = itemView.findViewById(R.id.movie_poster)
    private val title: TextView = itemView.findViewById(R.id.movie_title)
    private val overview: TextView = itemView.findViewById(R.id.movie_overview)


    fun bind(trendingMovie: Result) {
        Glide.with(itemView.context).load("http://image.tmdb.org/t/p/w500${trendingMovie.poster_path}").into(poster)
        title.text = "Title: " + trendingMovie.title
        overview.text = "Overview: " + trendingMovie.overview
        Log.d("bindTrending", "Movies: $Result")
    }
}