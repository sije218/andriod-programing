package com.example.movieapp10

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity


@Suppress("DEPRECATION")
class SplashScreen: AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val MovieLogoView = findViewById<ImageView>(R.id.MovieLogo)
        val animation = AnimationUtils.loadAnimation(this, R.anim.fade_in)

        MovieLogoView.visibility = View.VISIBLE
        MovieLogoView.startAnimation(animation)

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        Handler().postDelayed({
            val mainIntent = Intent(this, MainActivity::class.java)
            startActivity(mainIntent)
            finish()
        }, 3000)
    }
}