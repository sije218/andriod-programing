package com.example.movieapp10

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide


class MovieInfo : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movieinfo)
        val titleIntent = intent.getStringExtra("title")
        val overviewIntent = intent.getStringExtra("overview")
        val posterpathIntent = intent.getStringExtra("posterpath")
        val releasedateIntent = intent.getStringExtra("releasedate")
        val popularityIntent = intent.getStringExtra("popularity")

        val titleInfo = findViewById<TextView>(R.id.titleInfo)
        val overviewInfo = findViewById<TextView>(R.id.overviewInfo)
        overviewInfo.movementMethod = ScrollingMovementMethod()
        val posterInfo = findViewById<ImageView>(R.id.posterInfo)
        val releasedateInfo = findViewById<TextView>(R.id.releasedateInfo)
        val popularityInfo = findViewById<TextView>(R.id.popularityInfo)

        titleInfo.text = "Title: $titleIntent"
        overviewInfo.text = overviewIntent
        Glide.with(applicationContext).load("http://image.tmdb.org/t/p/w500$posterpathIntent").into(posterInfo)
        releasedateInfo.text = "Release date: \n $releasedateIntent"
        popularityInfo.text = "Popularity:\n $popularityIntent"

    }
}