package com.example.movieapp10


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import com.example.movieapp10.fragment.HomeFragment
import com.example.movieapp10.fragment.TrendingFragment
import kotlin.properties.Delegates

const val API: String = "8403d1ad0229a3c462fd27b3346b1e83"
var bredde by Delegates.notNull<Int>()
@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // --- Størrelse af skærmen
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        bredde = displayMetrics.widthPixels
        //--- SLUT

        val homeFragment = HomeFragment()
        val trendingFragment = TrendingFragment()

        makeCurrentFragment(homeFragment)


        findViewById<com.google.android.material.bottomnavigation.BottomNavigationView>(R.id.bottom_nav).setOnNavigationItemReselectedListener{
            when(it.itemId){
                R.id.ic_home -> makeCurrentFragment(homeFragment)
                R.id.ic_top100 -> makeCurrentFragment(trendingFragment)
            }
        }
    }
    private fun makeCurrentFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            commit()
        }
    }
}
