package com.example.movieapp10.fragment


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp10.fragment.movieGetter.Movie1
import com.example.movieapp10.fragment.movieGetter.MoviesRepository
import com.example.movieapp10.R
import com.example.movieapp10.bredde
import com.example.movieapp10.fragment.movieGetter.MovieAdapter1
import com.example.movieapp10.fragment.HomeFragment as HomeFragment

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    private lateinit var MovieAdapter: MovieAdapter1
    private lateinit var RanMovieRecyc: RecyclerView
    private lateinit var homeView: View
    val NrColumns: Int = bredde / 500 //Dividere bredden af skærmen med bredden af hver poster

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MovieAdapter = MovieAdapter1(listOf())
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeView = inflater.inflate(R.layout.fragment_home,container, false)
        RanMovieRecyc = homeView.findViewById(R.id.RanMovieRecyc)
        RanMovieRecyc.setHasFixedSize(true)
        RanMovieRecyc.layoutManager = GridLayoutManager(parentFragment?.context, NrColumns)
        RanMovieRecyc.adapter = MovieAdapter
        return homeView
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        MoviesRepository.getRanMovies(
                onSuccess = ::onRanMoviesFetched,
                onError = ::onError
        )
    }

    private fun onRanMoviesFetched(movies: List<Movie1>) {
        MovieAdapter.notifyDataSetChanged()
        MovieAdapter.updateMovies(movies)
        Log.d("RanMovie", "Movies: $movies")
    }

    private fun onError() {
        Toast.makeText(requireContext(), getString(R.string.FejlHentningAfFilm), Toast.LENGTH_SHORT).show()
    }
    override fun onDestroyView() {
        super.onDestroyView()

    }
}

